var func = function() {
    $("#submitBtn").prop('disabled',true);
    if ($('#email').val() != '') {
        url = '/checkemail/'+$('#email').val();
        console.log("Masuk sini")
        $("#submitBtn").prop('disabled',true);
        // console.log(url);
        $.ajax({
            url: url,
            success: function(response) {
                if (response.substring(4) == "NotAvailableBruv") {
                    $('#alert').addClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1 w-100');
                    $('#alert').html("Email pernah terdaftar");
                    $("#submitBtn").prop('disabled',true);
                } else if ($('#username').val() != "" && $('#password').val() != "" && $('#email').val() != "") {
                    $('#alert').removeClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1 w-100');
                    $('#alert').html("");
                    $("#submitBtn").prop('disabled',false);
                    // console.log(response.substring(4));
                }
            }
        });
    }
    if($('#username').val() == "" || $('#password').val() == "" || $('#email').val() == "") {
        $("#submitBtn").prop('disabled',true);
    }
}

var delet = function(id) {
    var email = $('#email-'+id).val();
    var password = prompt("Please enter your password","password")
    console.log($('#email-'+id).val());
    console.log(password)
        $.ajax({
            url: "/delete-subscriber/",
            type : "POST",
            data : {
                email : email,
                password : password,
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },
            success : function(response) {
                alert(response.message);
                if (response.message != "Wrong password") {
                    var i;
                    array = response.data;
                    var temp = "";
                    for(i = 0; i<array.length; i ++) {
                        temp += "<tr><td scope='row'>" + (i+1) + "</td>" +
                        "<td>" + array[i].name+ "</td>" +
                        "<td>" + array[i].email+ "</td>" +
                        "<td>" + array[i].registered+ "</td>" +
                        "<td><input id='email-" + i + "' type='hidden' value='"+ array[i].email +"'><button data-toggle='modal' data-target='#exampleModal' onclick='delet(" + i + ")' class='btn btn-danger'>Unsubsribe</button></td>" +
                        "</tr>"
                    }
                    $('#display').html(temp);

                }
            }
        });
}

var searchForm = null;
$(function(){
    $('#username').keyup(func);
    $('#email').keyup(func);
    $('#password').keyup(func);

    $('#displayBtn').on('click', function(e) {
        // console.log(e);
        $.ajax({
            url : '/display/',
            success: function(response) {
                var i;
                console.log("Masuk sini gan")
                array = response.users;
                var temp = "";
                for(i = 0; i<array.length; i ++) {
                    temp += "<tr><td scope='row'>" + (i+1) + "</td>" +
                    "<td>" + array[i].name+ "</td>" +
                    "<td>" + array[i].email+ "</td>" +
                    "<td>" + array[i].registered+ "</td>" +
                    "<td><input id='email-" + i + "' type='hidden' value='"+ array[i].email +"'><button onclick='delet(" + i + ")' class='btn btn-danger'>Unsubsribe</button></td>" +
                    "</tr>"
                }
                $('#display').html(temp);
            }
        });
    });
    $('#themeBtn').on('click', function() {
        var body = $('body');
        var button = $("#themeBtn");
        var link = $("#link");
        var eduBtn = $('#edubtn');
        var organisasibtn = $('#organisasibtn');
        console.log($('.card').css('background-color'));
        if (body.css("background-color") == "rgb(0, 255, 255)") {
            body.css({
                "background-color":"#222f3e",
                "color":"white"
            });
            button.css("background-color", "rgb(0, 255, 255)");
            link.html("Blue Theme");
            link.css("color","black");
            eduBtn.removeClass('btn-primary');
            eduBtn.addClass('btn-secondary');
            organisasibtn.removeClass('btn-primary');
            organisasibtn.addClass('btn-secondary');
            $('.card').removeClass('bg-white');
            $('.card').addClass('bg-dark');
        } else {
            body.css({
                "background-color":"rgb(0, 255, 255)",
                "color":"black",
            });
            link.html("Dark Theme");
            link.css("color","white");
            button.css("background-color", "#222f3e");
            eduBtn.removeClass('btn-secondary');
            eduBtn.addClass('btn-primary');
            organisasibtn.removeClass('btn-secondary');
            organisasibtn.addClass('btn-primary');
            $('.card').removeClass('bg-dark');
            $('.card').addClass('bg-white');
        }
    });

    $('#regisForm').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            type : "POST" ,
            url: "/inputEmail/",
            data: {
                name : $('#username').val(),
                email : $('#email').val(),
                password : $('#password').val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },
            success: function(response) {
                console.log($('#username').val());
                alert($('#username').val()+ " berhasil terdaftar");
                $('#username').val("");
                $('#email').val("");
                $('#password').val("");
            }
        });
        console.log("lewat");
    });
});
