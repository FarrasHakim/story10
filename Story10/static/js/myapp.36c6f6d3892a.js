var func = function() {
    $("#submitBtn").prop('disabled',true);
    if ($('#email').val() != '') {
        url = '/checkemail/'+$('#email').val();

        $("#submitBtn").prop('disabled',true);
        // console.log(url);
        $.ajax({
            url: url,
            success: function(response) {
                if (response.substring(4) == "NotAvailableBruv") {
                    $('#alert').addClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1');
                    $('#alert').html("Email pernah terdaftar");
                    $("#submitBtn").prop('disabled',true);
                } else if ($('#username').val() != "" && $('#password').val() != "" && $('#email').val() != "") {
                    $('#alert').removeClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1');
                    $('#alert').html("");
                    $("#submitBtn").prop('disabled',false);
                    // console.log(response.substring(4));
                }
            }
        });
    }
    if($('#username').val() == "" || $('#password').val() == "" || $('#email').val() == "") {
        $("#submitBtn").prop('disabled',true);
    }
}

var searchForm = null;
$(function(){
    $('#username').keyup(func);
    $('#email').keyup(func);
    $('#password').keyup(func);

    $('#themeBtn').on('click', function() {
        var body = $('body');
        var button = $("#themeBtn");
        var link = $("#link");
        console.log(button.css("background-color"));
        if (body.css("background-color") == "rgb(0, 255, 255)") {
            body.css({
                "background-color":"#222f3e",
                "color":"white"
            });
            button.css("background-color", "rgb(0, 255, 255)");
            link.html("Blue Theme");
            link.css("color","black");
        } else {
            body.css({
                "background-color":"rgb(0, 255, 255)",
                "color":"black",
            });
            link.html("Dark Theme");
            link.css("color","white");
            button.css("background-color", "#222f3e");
        }
    });

    $('#statusForm').on('submit', function(e) {
        e.preventDefault();
        if ($('#status').val() != "") {
            $.ajax({
                type : "POST" ,
                url: "/status/",
                data: {
                    status : $('#status').val(),
                    csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
                },
                success: function(response) {
                    location.reload();
                    $('#status').val("");
                }
            });
        } else {
            alert("Nothing to Post");
        }
    });

    $('#regisForm').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            type : "POST" ,
            url: "/status/",
            data: {
                name : $('#username').val(),
                email : $('#email').val(),
                password : $('#password').val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },
            success: function(response) {
                console.log("Form Submitted gan");
                alert($('#username').val()+ " berhasil terdaftar");
                $('#username').val("");
                $('#email').val("");
                $('#password').val("");
            }
        });
        console.log("lewat");
    });

    $('#del').on('click', function() {
        $.ajax({
            type : "POST" ,
            url: "/delete/",
            data: {
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },
            success: function(response) {
                $('#posts').html('No Recent Post');
            }
        });
    });

    $(document).keypress(function(e) {
      if(e.which == 13) {
        if (true) {
            if ($("#submitBtn").prop('disabled') != true) {
                $("#submitBtn").click();
                $('#username').val("");
                $('#email').val("");
                $('#password').val("");
            }
        }
      }
    });
});




