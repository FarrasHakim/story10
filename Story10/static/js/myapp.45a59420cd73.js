var func = function() {
    if ($('#email').val() != '') {
        url = '/checkemail/'+$('#email').val();

        $("#submitBtn").prop('disabled',true);
        // console.log(url);
        $.ajax({
            url: url,
            success: function(response) {
                if (response.substring(4) == "NotAvailableBruv") {
                    $('#alert').addClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1');
                    $('#alert').html("Email pernah terdaftar");
                    $("#submitBtn").prop('disabled',true);
                } else if ($('#username').val() != "" && $('#password').val() != "" && $('#email').val() != "") {
                    $('#alert').removeClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1');
                    $('#alert').html("");
                    // console.log();
                    $("#submitBtn").prop('disabled',false);
                    // console.log(response.substring(4));
                }
            }
        });
    }
    if($('#username').val() == "" || $('#password').val() == "" || $('#email').val() == "") {
        $("#submitBtn").prop('disabled',true);
    }
}

var searchForm = null;
$(function(){
    $('#username').keyup(func);
    $('#email').keyup(func);
    $('#password').keyup(func);
    $('#regisForm').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            type : "POST" ,
            url: "/inputEmail/",
            data: {
                name : $('#username').val(),
                email : $('#email').val(),
                password : $('#password').val(),
            },
            success: function(response) {
                alert($('#username').val()+ " berhasil terdaftar");
            }
        });
        $('#username').val("");
        $('#email').val("");
        $('#password').val("");
    });
    $(document).keypress(function(e) {
      if(e.which == 13) {
        if (true) {
            if ($("#submitBtn").prop('disabled') != true) {
                $("#submitBtn").click();
                $('#username').val("");
                $('#email').val("");
                $('#password').val("");
            }
        }
      }
    });
});




