from .models import User
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
import json
import urllib.request


def checkEmail(request, email):
    if (User.objects.all().count() == 0):
        return HttpResponse("<h2>isEmptyBruv")
    if User.objects.filter(email=email).count() > 0:
        return HttpResponse("<h2>NotAvailableBruv")
    return HttpResponse("<h2>AvailableBruv")


def deletDis(request):
    context = {
    }
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        obj = User.objects.filter(email=email)
        string = ""
        if password == obj[0].password:
            string = "Deleted " + obj[0].name
            obj.delete()
        else:
            string = "Wrong password"
        context['message'] = string
    data = list(User.objects.values())
    context['data'] = data
    return JsonResponse(context)


def display(request):
    return render(request, 'subscriber.html')


def displaySubs(request):
    users = list(User.objects.values())
    context = {
        'users': users,
    }
    return JsonResponse(context)


def inputUser(request):
    if request.method == "POST":
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        form = User(name=name, email=email, password=password)
        form.save()
        return HttpResponse("<h2>OK</h2>")
    return HttpResponse("<h2>404 Not Found</h2>")


def profile(request):
    return render(request, 'base.html')
