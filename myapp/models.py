from django.db import models

# Create your models here.


class User(models.Model):
    email = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    registered = models.DateField(auto_now_add=True, auto_now=False)
