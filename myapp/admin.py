from django.contrib import admin
from.models import User
# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_display = ["name", "email", "password", "registered"]
    list_filter = ["registered"]
    search_fields = ["name"]

    class Meta:
        model = User


admin.site.register(User, UserAdmin)
