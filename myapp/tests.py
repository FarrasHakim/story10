from .views import profile, inputUser, checkEmail
from .models import User
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
import time


class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        firefox_options = Options()
        firefox_options.add_argument('--headless')
        self.browser = webdriver.Firefox(firefox_options=firefox_options)

    def tearDown(self):
        self.browser.quit()

    def test_register(self):
        self.browser.get(self.live_server_url)
        # time.sleep(3)
        registBtn = self.browser.find_element_by_id('regisBtn')
        registBtn.click()
        nameField = self.browser.find_element_by_id('username')
        emailField = self.browser.find_element_by_id('email')
        passwordField = self.browser.find_element_by_id('password')
        nameField.send_keys("Test")
        emailField.send_keys("Test@Test")
        passwordField.send_keys("Test")
        # time.sleep(3)
        self.assertEqual(nameField.get_attribute('value'), "Test")
        self.assertEqual(emailField.get_attribute('value'), "Test@Test")
        self.assertEqual(passwordField.get_attribute('value'), "Test")
        submitButton = self.browser.find_element_by_id('submitBtn')
        submitButton.send_keys(Keys.RETURN)
        time.sleep(5)
        self.assertEqual(nameField.get_attribute('value'), "")
        self.assertEqual(emailField.get_attribute('value'), "")
        self.assertEqual(passwordField.get_attribute('value'), "")
        self.tearDown()

    def test_change_theme(self):
        self.browser.get(self.live_server_url)
        body = self.browser.find_element_by_tag_name('body')
        bgbody = body.value_of_css_property('background-color')
        self.assertEqual(bgbody, 'rgb(0, 255, 255)')
        themeBtn = self.browser.find_element_by_id('themeBtn')
        themeBtn.click()
        bgNow = body.value_of_css_property('background-color')
        self.assertEqual(bgNow, 'rgb(34, 47, 62)')
        self.tearDown()


class Story9UnitTest(TestCase):
    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_function_profile(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'base.html')

    def test_url_checkEmail_is_exist(self):
        email = "asd@asd"
        response = Client().get('/checkemail/' + email)
        self.assertEqual(response.status_code, 200)

    def test_add_user(self):
        request = {
            'name': "Test",
            'email': "Test@Test",
            'password': "Test",
        }
        response = Client().post('/inputEmail/', request)
        html_response = response.content.decode('utf8')
        self.assertIn("OK", html_response)
        self.assertEqual(response.status_code, 200)

    def test_function_checkEmail(self):
        email = "asd@asd"
        found = resolve('/checkemail/' + email)
        self.assertEqual(found.func, checkEmail)

    def test_add_new_user_email_while_db_is_empty(self):
        email = "asd@asd"
        response = Client().get('/checkemail/' + email)

        html_response = response.content.decode('utf8')
        self.assertIn("isEmptyBruv", html_response)

    def test_add_existing_email(self):
        User.objects.create(name="asd@asd", email="asd@asd", password="asd@asd")
        response = Client().get('/checkemail/' + "asd@asd")
        html_response = response.content.decode('utf8')
        self.assertIn("NotAvailableBruv", html_response)

    def test_add_new_user_email_while_db_is_not_empty(self):
        User.objects.create(name="asd@asd", email="asd@asd", password="asd@asd")
        new_email = "awd@awd"
        response = Client().get('/checkemail/' + new_email)
        html_response = response.content.decode('utf8')
        self.assertIn("AvailableBruv", html_response)

    def test_url_input_email_exist(self):
        response = Client().get('/inputEmail/')
        self.assertEqual(response.status_code, 200)

    def test_function_input_email(self):
        found = resolve('/inputEmail/')
        self.assertEqual(found.func, inputUser)
